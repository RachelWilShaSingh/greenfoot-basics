# Greenfoot Basics

Greenfoot projects and reference

------------

# Creating a project

1. In Greenfoot's top menu, click on **Scenario** and then **New Java Scenario** to get started.
2. Give your scenario a **Name** and choose a **Location** on your hard drive, then click **OK**.
3. When the project is first created, you'll have one **MyWorld** and no **Actors**.

## Adding actors

To create a new actor, right click on the **Actor** box on the right sidebar and click **New Subclass**.
Here, you can give your new actor a name and choose an image.

## Editing an actor's code

To edit the code of an actor, right-click on the actor's box that you want, then click **Open Editor**.
You will see a function called **act()** already there, which is where most of your code will go.

When writing code for this actor, make sure you're adding code between the opening **{** (immediately after **act()**) and closing **}**.

## Making an actor always appear

To make an actor always appear during reset, open the **MyWorld** editor.
Let's say your actor is named **Potato**. Your code will look like this:

```java
public class MyWorld extends World
{
    public MyWorld()
    {    
        super(800, 600, 1); 
        
        addObject( new Potato(), 400, 350 );
    }
}
```

Where *(400, 350)* is the starting *(x, y)* coordinate for the Potato.

------------

# Coding Cookbook

## Moving an actor forward - void move( int )

To move a character forward, you can use the **move** function like this:

```java
move( 50 );
```

Where the number within the parentheses () is the amount of *pixels* to move the actor.

## Turning an actor - void turn( int )

To turn a character, you can use the **turn** function like this:

```java
turn(45);
```

Where the number within the parentheses () is the angle, in degrees.

* 45 degrees - diagonal
* 90 degrees - right angles
* 180 degrees - opposite
* 360 degrees - full circle

## Checking for keyboard presses - bool Greenfoot.isKeyDown( string )

To check whether a certain key is being pressed, you will use the **bool Greenfoot.isKeyDown( string )** function.

```java
if ( Greenfoot.isKeyDown( "w" ) )
{
    // Code to execute if "w" is pressed.
}
```

You can use any keyboard letter (a-z) here, as well as the directions "up", "down", "left", "right" to correspond to the keyboard's arrow keys.

**This should go within the act() function!**

## Moving a character - Spaceship movement

Spaceship movement style is where you press LEFT and RIGHT to rotate the actor,
UP to move forward, and DOWN to move backward. It works like this...

```java
public void act()
{
    if      ( Greenfoot.isKeyDown( "left" ) )   // Turn
    {
        turn( -1 );
    }
    else if ( Greenfoot.isKeyDown( "right" ) )  // Turn
    {
        turn( 1 );
    }
    
    if      ( Greenfoot.isKeyDown( "up" ) )     // Forward
    {
        move( 5 );
    }
    else if ( Greenfoot.isKeyDown( "down" ) )   // Backward
    {
        move( -5 );
    }
    
}
```

## Moving a character - Directional movement

With directional movement, when you press UP the actor will go UP on the screen, and the same for
the other directions as well. This requires knowing where the actor's *(x, y)* coordinate currently is,
and modifying it.

```java
public void act()
{
    // Get the current (x, y) coordinate
    int x = getX();
    int y = getY();

    // Update the variable values
    if ( Greenfoot.isKeyDown( "up" ) ) 
    {
        y = y - 5;
    }
    else if ( Greenfoot.isKeyDown( "down" ) )
    {
        y = y + 5;
    }

    if ( Greenfoot.isKeyDown( "left" ))
    { 
        x = x - 5;
    }

    if ( Greenfoot.isKeyDown( "right" ))
    { 
        x = x + 5;
    }
    
    // Update the image's (x, y) coordinate on the screen
    setLocation( x, y );
    
}
```

## Collision detection - are two actors hitting?

Collision detection is to see whether two images on the screen are touching.
This might be useful if your character collects coins during gameplay, or
needs to avoid enemies, and so on.

Let's say you have an actor type called **Coin** and you want to check if your
character **Player** is touching the coin. In this case, you would edit
**Player's act() function**:

```java
public void act()
{
    Coin collision = getOneIntersectingObject( Coin.class );
    
    if ( collision != null )
    {
        // Remove the coin
        getWorld().removeObject( collision );
    }
}
```

